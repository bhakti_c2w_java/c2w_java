/*
1. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
C B A
3 3 3
C B A
row=4
D C B A
4 4 4 4
D C B A
4 4 4 4
*/

import java.io.*;

class SquarePattern{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			int ch = 64+rows;
			for(int j=1; j<=rows; j++){
			
				if(i%2==0){
				
					System.out.print(rows + "   ");
				} else {
				
					System.out.print((char)ch + "   ");
					ch--;
				}
			}
			System.out.println("\n");
		}
	}
}
