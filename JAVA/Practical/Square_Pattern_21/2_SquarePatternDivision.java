/*
2. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
9 4 25
18 7 8
27 50 11

row=4
4 25 18 7
8 27 50 11
36 13 14 45
16 17 54 19
*/

import java.io.*;

class SquarePattern2{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;
		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				if(num%3==0){
				
					System.out.print(num*3 +"  ");
				} else if(num%5==0){
				
					System.out.print(num*5 +"  ");
				} else {
				
					System.out.print(num +"  ");
				}
				num++;
			}
			System.out.println("\n");
		}
	}
}
