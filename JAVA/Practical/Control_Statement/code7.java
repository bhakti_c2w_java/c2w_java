class SwitchDemo2{
	public static void main(String[] args){
		int X = 50;	
		System.out.println("Start code");

		switch (X){
			case 20+40:
				System.out.println("20+40");
				break;
			case 25+50:
				System.out.println("25+50");
				break;
			case 50:
				System.out.println("50");
				break;
			default:
				System.out.println("In default state");
		}
		System.out.println("End code");
	}

}
