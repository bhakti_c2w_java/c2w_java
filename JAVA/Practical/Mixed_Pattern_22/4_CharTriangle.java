/*
4. WAP in notebook & Dry run first then type
Take number of rows from user : (check the row is even or odd if odd print Capital
letters else print small letters)
row=3
A B C
  B C
    C

row=4
a b c d
  b c d
    c d
      d
*/

import java.io.*;

class CharTriangle{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.print("No. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		
		for(int i=1; i<=rows; i++){
	
			char capital = 'A';
                        char small = 'a';

			for(int space=1; space<i; space++){
			
				System.out.print("   ");
				small++;
                                capital++;
			}

			for(int j=1; j<=rows-i+1; j++){
			
				if(rows%2==0){ 
				
					System.out.print(small +"  ");
				} else {
				
					System.out.print(capital +"  ");
				}
				small++;
				capital++;
			}
			
			System.out.println("\n");
		}
	}
}
