/*
3. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

       1
     4 7
10 13 16
row=4

          1
        5 9
   13 17 21
25 29 33 37
      */

import java.io.*;

class Triangle3{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("No. Of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = 1;
		for(int i=1; i<=rows; i++){
		
			for(int space=1; space<=rows-i; space++){
			
				System.out.print("    ");
			}

			for(int j=1; j<=i; j++){
			
				System.out.print(num +"  ");
				num+=rows;
			}

			System.out.println("\n");
		}
		
	}
}
