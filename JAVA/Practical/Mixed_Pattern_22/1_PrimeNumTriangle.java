/*
1. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
     1
  3  5
7 9 11

row=4

          1
       3  5
    7  9 11
13 17 19 23
*/

import java.util.*;

class Triangle1{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter no. of rows : ");
		int rows = sc.nextInt();

		int num = 1;

		for(int i=1; i<=rows; i++){

			for(int sp=1; sp<=rows-i; sp++){
			
				System.out.print("    ");
			}		

			
			for(int j=1; j<=i; j++){

		
				int x = 2;
				while(x<num/2){

					if(num%x==0){
						System.out.print(num +"  ");
					
					}
			
                                        x++;
				}
				num++;
			}
			
			System.out.println("\n");
		}
	}
}

