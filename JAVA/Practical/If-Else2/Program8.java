class Percentage{
	public static void main(String[] args){
		float Percent = 60.00f;

		if (Percent>70.00 && Percent<=90.00){
			System.out.println("First class with Distinction");
		}
		else if (Percent>60.00 && Percent<=70.00) {
			System.out.println("First Class");
		}
		else if (Percent>=50 && Percent<=40){
			System.out.println("Pass");
		}
		else {
			System.out.println("Fail");
		}
	}
}
