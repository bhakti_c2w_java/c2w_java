/*
10. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3

c b a
B A
a
Rows = 4
D C B A
c b a
B A
a
*/

import java.io.*;

class CapitalSmall{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int chCapital = 64+rows;
		int chSmall = 96+rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=rows; j>=i; j--){
			
				if(rows%2==0){
				
					if(i%2==0){
			
					
						System.out.print((char)chSmall+"  ");
						chSmall--;
					} else {
				
						System.out.print((char)chCapital+"  ");
						chCapital--;
					}	
				} else {
					
				 	if(i%2==1){

                                                System.out.print((char)chSmall+"  ");
                                                chSmall--;
                                        } else {

                                                System.out.print((char)chCapital+"  ");
                                                chCapital--;
                                        }
				}
			}
			System.out.println("\n");

			chCapital = 64+rows-i;
			chSmall = 96+rows-i;
		}
	}
}
