/*
1. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
1 2 3 4
2 3 4
3 4
4
Rows = 5
1 2 3 4 5
2 3 4 5
3 4 5
4 5
5
*/

import java.io.*;

class NumIcrement{

	public static void main(String n[])throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	

		System.out.print("Enter no. of Row : ");
		int rows = Integer.parseInt(br.readLine());
			
		for(int i=1; i<=rows; i++){
		
			int num = i;

			for(int j=rows; j>=i; j--){
			
				System.out.print(num++ +"  ");
			}

			
			System.out.println("\n");
		}

	}
}
