/*
8. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
4 3 2 1
C B A
2 1
A
Rows = 5
5 4 3 2 1
D C B A
3 2 1
B A
1
*/

import java.io.*;

class Triangle8{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;
		int ch = 64+rows;

		for(int i=1; i<=rows;i++){
		
			for(int j=1; j<=rows-i+1; j++){
			
				if(i%2==0){
				
					System.out.print((char) ch +"  ");
				} else {
				
					System.out.print(num +"  ");
				}
				ch--;
				num--;
			}
			System.out.println("\n");
			
			num = rows-i;
			ch = 64+rows-i;
		}
	}
}
