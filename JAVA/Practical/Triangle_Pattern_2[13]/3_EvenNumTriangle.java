/*
3. WAP in notebook & Dry run first then type
Take number of rows from user :

Rows = 4
20 18 16 14
12 10 8
6 4
2
Rows = 5
30 28 26 24 22
20 18 16 14
12 10 8
6 4
2
*/

import java.io.*;

class EvenNum3{

	public static void main (String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows *(rows+1);
		for(int i=1; i<=rows; i++){

			for(int j=rows; j>=i; j--){
			
				System.out.print(num +"  ");
				num -=2;
			}
			System.out.println("\n");
			
		}
	}
}
