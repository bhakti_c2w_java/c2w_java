/*
6. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1 a 2
1 a
1
Rows = 4
1 a 2 b
1 a 2
1 a
1
*/

import java.io.*;

class ColumnWise{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			char ch ='a';
			int num = 1;

			for(int j=rows; j>=i; j--){
		
				if(j%2==1){
				
					System.out.print(ch +"  ");
					ch++;
				} else {
				
					System.out.print(num +"  ");
					num++;
				}
				
			}
			System.out.println("\n");
		}
	}
}
