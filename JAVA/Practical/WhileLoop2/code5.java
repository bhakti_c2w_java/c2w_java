class Division{
	public static void main(String[] args){
		int num = 216985;
		System.out.println("Cubes of even digits:" +" ");
		while(num>0){
			int rem=num%10;
			if(rem%2==0){
				System.out.print(rem*rem*rem +" ");
			}
			num/=10;
		}
		System.out.println();
	}
}
