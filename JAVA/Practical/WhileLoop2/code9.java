class Square{
	public static void main(String[] args){
		int num=2469185;
		int sum=0;
		System.out.print("sum of square of odd digits: ");
		while(num>0){
			int rem=num%10;
		       if(rem%2==1){
		       sum+=rem*rem;
		       }	
		       num/=10;
		}
		System.out.println(sum);
	}
}
