class Sum{
	public static void main(String[] args){
		int num=9367924;
		int sum=0;
		int prod=1;
		while(num>0){
			int rem=num%10;
			if(rem%2==0){
			prod*=rem;
			}else{
				sum+=rem;
			}
			num/=10;
		}
		System.out.println("sum of odd digits: "+sum);
		System.out.println("Product of even digits: "+prod);
	}
}
