/*
 Row=4

 1 2 3 4
 1 2 3 
 1 2 
 1
 */

import java.io.*;
class Triangle{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Rows :");
		int row = Integer.parseInt(br.readLine());

		for(int i=row; i>=1; i--){
			int num = 1;
			for(int j =1; j<=i; j++){
			System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}
