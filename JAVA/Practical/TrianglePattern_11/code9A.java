/*
 Row = 4
 D C B A
 D C B
 D C
 D
 */

import java.io.*;
class Triangle{
	public static void main(String[] args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Rows :");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			int ch=64+row;
			for(int j=row; j>=i; j--) {
				System.out.print((char) ch +  " " );
				ch--;
			}
			System.out.println();
		}
	}
}

