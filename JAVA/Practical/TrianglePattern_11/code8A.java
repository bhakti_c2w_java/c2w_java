
/*
 Row=4
 1 2 3 4
 2 3 4
 3 4
 4
 */
import java.io.*;
class Triangle{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Row : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++) {
			for(int j=i; j<=row; j++) {
				System.out.print(j + " ");
			}
				System.out.println();
		}
	}
}
