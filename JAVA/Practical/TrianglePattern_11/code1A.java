/*
WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1
1 2
1 2 3

*/

import java.io.*;
class Triangle{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Row: ");
		int row = Integer.parseInt(br.readLine());
			for(int i=1;i<=row;i++){
				int num=1;
				for(int j=1;j<=i;j++){
					System.out.print(num + " ");
					num++;
				}
				System.out.println();
			}
	}
}
