/*
 Row = 3
 3 3 3
 2 2
 1
 */

import java.io.*;
class Triangle{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter the Rows :");
		
		int row = Integer.parseInt(br.readLine());
		for(int i = row; i>=1; i--){
			for(int j = 1; j<=i;j++){
				System.out.print(i + " ");
			}
			System.out.println();
		}
	}
}
	



