/*
 Row=4
 4
 4 8
 4 8 12
 4 8 12 16
*/

import java.io.*;

class Triangle{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter the Rows :");
			int row = Integer.parseInt(br.readLine());
	
			for(int i = 1; i<=row; i++){
				int num=1;
				for(int j = 1; j<=i; j++){
					System.out.print(num*row +" ");
					num++;
				}

					System.out.println();
			}
	}
}
