class SwitchDemo{
	public static void main(String[] args){
		int x = 50;
		switch(x){
			case 20+30:
				System.out.println("20+30");
				break;
			case 25+30:
				System.out.println("55");
				break;
			default:
				System.out.println("Default");
		}
	}

}
